<?php

require_once '../config/db.php';
require_once '../Classes/Subject.php';
require_once '../classes/Teacher.php';

$id = htmlspecialchars($_GET['subId']);
$subject = Subject::getById($id, $pdo);
$teachersArr = $subject->getIdTeachers();

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?=$subject->getTitle()?></title>
</head>
<body>
<h1>Subject name - <?=$subject->getTitle()?>.</h1>
<!--<ul>-->
<!--	--><?php //foreach ($subject->getTeachers() as $teacher):?>
<!--		<li><a href="../teachers/infoTeacher.php?id=--><?//=$teacher->getId();?><!--">--><?//=$teacher->getFullName();?><!--</a></li>-->
<!--	--><?php //endforeach;?>
<!--</ul>-->
<ul>
	<?php foreach ($teachersArr as $teacherArr):?>
		<?php  $teacher = Teacher::getById($teacherArr['teacher_id'],$pdo);?>
		<li><?= $teacher->getName(); ?></li>
	<?php endforeach;?>
</ul>
</body>
</html>
