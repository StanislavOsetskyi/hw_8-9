<?php
if(empty($_POST['id'])){
	header('Location:index.php');
}

require_once '../config/db.php';
require_once '../Classes/Subject.php';

Subject::delete($_POST['id'], $pdo);
header('Location: allSubjects.php');