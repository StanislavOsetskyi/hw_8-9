<?php
require_once '../config/db.php';
require_once '../Classes/Subject.php';

if (!empty($_POST['id'])) {

	Subject::upgrade($_POST['id'], $_POST['title'],$pdo);
}
header('Location: allSubjects.php');