<?php
require_once '../config/db.php';
require_once '../Classes/Subject.php';

$subjects = Subject::all($pdo);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Subjects list</title>
</head>
<body>
<div>
	<table>
		<?php foreach ($subjects as $subject):?>
			<tr>
				<td><?= $subject->getTitle()?></td>
				<td>
					<form action="editSubject.php" method="get">
						<input type="hidden" name="id" value="<?=$subject->getId()?>">
						<button>Edit</button>
					</form>
				</td>
				<td>
					<form action="deleteSubject.php" method="post">
						<input type="hidden" name="id" value="<?=$subject->getId()?>">
						<button>Delete</button>
					</form>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<div>
	<tr>
		<td>
			<form action="createSubject.php" method="post">
				<button>Add new subject</button>
			</form>
		</td>
	</tr>
    <td><a href="../index.php"><button>Back</button></a></td>
</div>
</body>
</html>