<?php
require_once '../config/db.php';
require_once '../Classes/Subject.php';

if(!empty($_POST['title'])){
	$title = htmlspecialchars($_POST['title']);
	$subject = new Subject($title);
	$subject->store($pdo);
}
header('Location: allSubjects.php');
