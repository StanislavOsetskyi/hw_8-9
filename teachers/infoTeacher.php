<?php
if(empty($_GET['id'])) {
	header('Location:404.php');
	die();
}

require_once '../config/db.php';
require_once '../Classes/Teacher.php';
require_once '../Classes/Subject.php';

$id = htmlspecialchars($_GET['id']);
$teacher = Teacher::getById($id, $pdo);
$subjectsArr = $teacher->getIdSubjects();


?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?=$teacher->getFullName()?></title>
</head>
<body>
	<h1><?=$teacher->getFullName()?>.</h1>
	<ul>
		<?php foreach ($subjectsArr as $subjectArr):?>
			<?php  $subject = Subject::getById($subjectArr['subject_id'],$pdo);?>
			<li><?= $subject->getTitle(); ?></li>
		<?php endforeach;?>
	</ul>
</body>
</html>
