<?php
require_once '../config/db.php';
require_once '../Classes/Department.php';
require_once '../Classes/Subject.php';

$departments = Department::all($pdo);
$subjects = Subject::all($pdo);


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New teacher</title>
</head>
<body>
<h1>Add new teacher</h1>
<form action="storeTeacher.php" method="post">
    <div>
        <label>Name: <input type="text" name="name"></label>
    </div>
    <div>
        <label>Surname: <input type="text" name="surname"></label>
    </div>
    <div>
        <label>Email: <textarea name="email"></textarea></label>
    </div>
    <div>
        <label for="departments">Select department:</label>
        <select name="department_id" id="" >
            <?php foreach ($departments as $department):?>
                <option value="<?=$department->getId()?>">  <?=$department->getTitle()?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="subjects">Select Subjects:</label>
            <select multiple   size="4" name="subjectsId[]">
		        <?php foreach ($subjects as $subject):?>
                    <option value="<?= $subject->getId() ?>"><?=$subject->getTitle()?></option>
		        <?php endforeach; ?>
            </select>
    </div>
    <button>Save</button>
</form>
</html>
