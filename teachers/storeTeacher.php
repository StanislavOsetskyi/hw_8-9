<?php
require_once '../config/db.php';
require_once '../Classes/Teacher.php';

if (!empty($_POST['name'])){
	$name = htmlspecialchars($_POST['name']);
	$surname = htmlspecialchars($_POST['surname']);
	$email = htmlspecialchars($_POST['email']);
	$departmentId = htmlspecialchars($_POST['department_id']);
    $teacher = new Teacher($name,$surname,$email,$departmentId);
    $teacher->store($_POST['subjectsId']);
}

header('Location:../index.php');