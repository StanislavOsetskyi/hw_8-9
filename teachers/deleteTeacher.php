<?php

if (empty($_GET['id'])) {
	header('Location:index.php');
}

require_once '../config/db.php';
require_once '../Classes/Teacher.php';

Teacher::delete($_GET['id'], $pdo);
header('Location:../index.php');