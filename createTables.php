<?php
require_once 'config/db.php';

try {
    $departmentsSql = "
	CREATE TABLE departments(
	    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	    title VARCHAR (255) NOT NULL,
	    phone VARCHAR (255) NOT NULL
    ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;
";
    $pdo->exec($departmentsSql);

    $teachersSql = "CREATE TABLE teachers(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR (255) NOT NULL,
    surname VARCHAR (255) NOT NULL,
    email VARCHAR (255) NOT NULL,
    department_id INT,
    FOREIGN KEY(department_id)REFERENCES departments(id) ON DELETE CASCADE 
    ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;";
    $pdo->exec($teachersSql);

    $subjectsSql = "CREATE TABLE subjects(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR (255) NOT NULL
    )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;";
    $pdo->exec($subjectsSql);

	$communicationsSql = "
	CREATE TABLE subject_teacher(
	    teacher_id INT,
	    subject_id INT,
	    FOREIGN KEY (teacher_id) REFERENCES teachers (id) ON DELETE CASCADE,
		FOREIGN KEY (subject_id) REFERENCES subjects (id) ON DELETE CASCADE
    ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;
";
	$pdo->exec($communicationsSql);


}catch (Exception $exception){
    echo "Error creating table! " . $exception->getCode() . ' message: ' . $exception->getMessage();
    die();
}



