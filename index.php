<?php
require_once 'config/db.php';
require_once 'Classes/Teacher.php';
require_once 'Classes/Subject.php';

$teachers = Teacher::all($pdo);

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
<div>
	<table>
		<tr>
			<td><a href="departments/allDepartments.php">Departments</a></td>
		</tr>
		<tr>
			<td><a href="teachers/addTeacher.php">add teacher</a></td>
		</tr>
		<tr>
			<td><a href="subjects/allSubjects.php">subjects</a></td>
		</tr>
	</table>
</div>
    <table>
        <thead>
            <tr>
                <th>Teacher Name</th>
                <th></th>
                <th>Department</th>
                <th></th>
                <th>Subjects</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($teachers as $teacher):?>
            <tr>
                <td><a href="teachers/infoTeacher.php?id=<?=$teacher->getId()?>"><?=$teacher->getFullName()?></a></td>
                <td></td>
                <td><a href="departments/show.php?id=<?=$teacher->getDepartment()->getId()?>"><?=$teacher->getDepartment()->getTitle()?></a></td>
                <td></td>
                <td>
		            <?php foreach ($teacher->subjects() as $subject): ?>
                        <a  href="subjects/details.php?subId=<?= $subject['obj']->getId() ?>" ><?= $subject['obj']->getTitle() ?></a>
		            <?php endforeach; ?>
                </td>
                <td></td>
                <td><a href="teachers/deleteTeacher.php?id=<?=$teacher->getId()?>">Delete</a></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</body>
</html>
