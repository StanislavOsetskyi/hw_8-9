<?php

require_once 'DBEntity.php';

class Subject extends DBEntity
{
    protected $id;
    protected $title;

    public function __construct($title)
    {
	    parent::__construct();
        $this->title = $title;
    }

    static public function all(PDO $pdo){
        try {
            $sql = "SELECT * FROM subjects";
            $statement = $pdo->query($sql);
	        $subjectsArray = $statement->fetchAll();
            $subjectObj = [];
            foreach ($subjectsArray as $subjectArray){
	            $subject = new self($subjectArray['title']);
	            $subject->setId($subjectArray['id']);
	            $subjectObj[] = $subject;
            }
            return $subjectObj;

        }catch(Exception $a){

        }
    }

	public function store(PDO $pdo)
	{
		try {
			$sql = 'INSERT INTO subjects SET 
            title = :title;
        ';
			$statement = $pdo->prepare($sql);
			$statement->bindValue(':title', $this->getTitle());
			$statement->execute();

		} catch (Exception $exception) {
			echo "Error storing product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
			die();
		}
	}

	static public function getById($id, $pdo){
		try {
			$sql = "SELECT * FROM subjects WHERE id=:id";
			$statement = $pdo->prepare($sql);
			$statement->bindValue(':id', $id);
			$statement->execute();
			$subjectArr = $statement->fetchAll();
		}catch (Exception $exception){
			die('Error getting Department'.$exception->getCode());
		}
		$subjectObj = new self($subjectArr[0]['title']);
		$subjectObj->setId($subjectArr[0]['id']);
		return $subjectObj;
	}

	public function getIdTeachers(){
		try{
			$statement = $this->db->prepare('SELECT teacher_id FROM subject_teacher WHERE subject_id=:id');
			$statement->bindValue(':id', $this->id);
			$statement -> execute();
			$idArr = $statement->fetchAll();
		}catch (Exception $exception) {
			die('Error getting teacher' . $exception->getCode(). $exception->getMessage()) ;
		}

		return $idArr;
	}

	static public function upgrade($id, $title,Pdo $pdo){
		$product = new Subject($title);
		$product->setId($id);
		$product->update($pdo);
	}

	public function update(PDO $pdo){
		try {
			$sql = 'UPDATE subjects SET
            title = :title
            WHERE id = :id';


			$statement = $pdo->prepare($sql);
			$statement->execute([
				':title' => $this->title,
				':id' => $this->id,
			]);

		}catch (Exception $exception){
			echo "Error updating product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
			die();
		}
	}

	static public function delete($id, PDO $pdo){
		$department = self::getById($id, $pdo);
		$department->destroy($pdo);
	}
	
	public function destroy($pdo){
		try {
			$sql = "DELETE FROM subjects WHERE id=:id";
			$statement = $pdo->prepare($sql);
			$statement->bindValue(':id', $this->id);
			$statement->execute();
		}catch (Exception $exception){
			echo "Error deleting product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
			die();
		}
	}

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setId($id)
    {
        $this->id = $id;
    }



}