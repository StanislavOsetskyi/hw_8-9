<?php
require_once 'DBEntity.php';
require_once 'Department.php';
require_once 'Subject.php';

class Teacher extends DBEntity
{
    protected $id;
    protected $name;
    protected $surname;
    protected $email;
    protected $departmentId;

    public function __construct($name, $surname, $email, $departmentId=null)
    {
	    parent::__construct();
        $this->name = htmlspecialchars($name);
        $this->surname = htmlspecialchars($surname);
        $this->email = htmlspecialchars($email);
        $this->departmentId = htmlspecialchars($departmentId);
    }

    public function store($subjectsId)
    {
        try {
            $sql = 'INSERT INTO teachers SET
            name = :name,
            surname = :surname,
            email = :email,
            department_id = :department_id';

            $statement = $this->db->prepare($sql);
            $statement->bindValue(':name', $this->getName());
            $statement->bindValue(':surname', $this->getSurname());
            $statement->bindValue(':email', $this->getEmail());
            $statement->bindValue(':department_id', $this->getDepartmentId());
            $statement->execute();
	        $this->id = $this->db->lastInsertId();

        } catch (Exception $exception) {
            echo "Error storing product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }

	    foreach ($subjectsId as $subjectId){
		    $this->addSubject($subjectId);
	    }
    }


	public function addSubject($subjectId){
		try{
			$subjectTeacherStatement = $this->db->prepare("INSERT INTO subject_teacher SET
                teacher_id = :teacher_id,
                subject_id = :subject_id
            ");
			$subjectTeacherStatement->bindValue(':teacher_id', $this->id);
			$subjectTeacherStatement->bindValue(':subject_id', $subjectId);
			$subjectTeacherStatement->execute();
		}catch (Exception $e){
			die('Error while saving order_product! '.$e->getMessage());
		}
	}

    static public function all(PDO $pdo){
        try {
            $sql = 'SELECT * FROM teachers';
            $statement = $pdo->query($sql);
            $teachersArr = $statement->fetchAll();
            $teacherObjs =[];

            foreach ($teachersArr as $teacherArr ){
                $teacherObj = new self($teacherArr['name'], $teacherArr['surname'],
                    $teacherArr['email'],$teacherArr['department_id']);
                $teacherObj->setId($teacherArr['id']);
                $teacherObjs[] = $teacherObj;
            }

            return $teacherObjs;

        }catch (Exception $exception){
            echo "Error getting products! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

	static public function getById($id, $pdo){
		try {
			$sql = "SELECT * FROM teachers WHERE id=:id";
			$statement = $pdo->prepare($sql);
			$statement->bindValue(':id', $id);
			$statement->execute();
			$teacherArr = $statement->fetchAll();
		}catch (Exception $exception){
			die('Error getting teacher'.$exception->getCode());
		}
		$teacherObj = new self($teacherArr[0]['name'],$teacherArr[0]['surname'],$teacherArr[0]['email'],$teacherArr[0]['department_id']);
		$teacherObj->setId($teacherArr[0]['id']);
		return $teacherObj;
	}

	public function getIdSubjects(){
		try{
			$statement = $this->db->prepare('SELECT subject_id FROM subject_teacher WHERE teacher_id=:id');
			$statement->bindValue(':id', $this->id);
			$statement -> execute();
			$idArr = $statement->fetchAll();
		}catch (Exception $exception) {
			die('Error getting teacher' . $exception->getCode(). $exception->getMessage()) ;
		}

		return $idArr;
	}

	public function subjects(){
		$sql ="SELECT * FROM subject_teacher  st
        LEFT OUTER JOIN subjects  s ON st.subject_id = s.id
        WHERE st.teacher_id =".$this->id;
		$statement = $this->db->query($sql);
		$teacherSubject = [];
		foreach ($statement->fetchAll() as $teacherSubjectArr) {
			$subject = [
				'obj' => new Subject($teacherSubjectArr['title'])
			];
			$subject['obj']->setId($teacherSubjectArr['subject_id']);
			$teacherSubject[] = $subject;

		}
		return $teacherSubject;
	}

	static public function delete($id, PDO $pdo){
		$department = self::getById($id, $pdo);
		$department->destroy($pdo);
	}

	public function destroy($pdo){
		try {
			$sql = "DELETE FROM teachers WHERE id=:id";
			$statement = $pdo->prepare($sql);
			$statement->bindValue(':id', $this->id);
			$statement->execute();
		}catch (Exception $exception){
			echo "Error deleting product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
			die();
		}
	}

	public function getDepartment()
	{
		$statement = $this->db->prepare('SELECT * FROM departments WHERE id=:id');
		$statement->bindValue(':id', $this->departmentId);
		$statement->execute();
		$categoryArr = $statement->fetchAll();
		$department = new Department($categoryArr[0]['title'],$categoryArr[0]['phone']);
		$department->setId($categoryArr[0]['id']);
		return $department;
	}

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getFullName()
    {
    	return $this->getName().' '. $this->getSurname();
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getDepartmentId()
    {
        return $this->departmentId;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


}