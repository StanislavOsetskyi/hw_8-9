<?php

require_once 'DBEntity.php';

class Department extends DBEntity
{
    protected $id;
    protected $title;
    protected $phone;

    public function __construct($title, $phone)
    {
	    parent::__construct();
        $this->title = htmlspecialchars($title);
        $this->phone = htmlspecialchars($phone);
    }

    static public function all(PDO $pdo){
        try {
            $sql = "SELECT * FROM departments";
            $statement = $pdo->query($sql);
            $departmentsArray = $statement->fetchAll();
            $departmentObj = [];
            foreach ($departmentsArray as $departmentArray){
                $department = new self($departmentArray['title'],$departmentArray['phone']);
                $department->setId($departmentArray['id']);
                $departmentObj[] = $department;
            }
            return $departmentObj;

        }catch(Exception $a){

        }
    }

	public function getTeachers(){
		$sql = "SELECT * from teachers WHERE department_id =".$this->id;
		$statement = $this->db->query($sql);
		$teachersArr = $statement->fetchAll();
		$teachersObjs = [];
		foreach ($teachersArr as $teacher){
			$teacherObj = new Teacher($teacher['name'], $teacher['surname'], $teacher['email'], $teacher['department_id']);
			$teacherObj->setId($teacher['id']);
			$teachersObjs[] = $teacherObj;
		}
		return $teachersObjs;
	}

	public function store(PDO $pdo)
	{
		try {
			$sql = 'INSERT INTO departments SET 
            title = :title,
            phone = :phone;
        ';
			$statement = $pdo->prepare($sql);
			$statement->bindValue(':title', $this->getTitle());
			$statement->bindValue(':phone', $this->getPhone());
			$statement->execute();

		} catch (Exception $exception) {
			echo "Error storing product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
			die();
		}
	}

	static public function getById($id, $pdo){
		try {
			$sql = "SELECT * FROM departments WHERE id=:id";
			$statement = $pdo->prepare($sql);
			$statement->bindValue(':id', $id);
			$statement->execute();
			$departmentArr = $statement->fetchAll();
		}catch (Exception $exception){
			die('Error getting Department'.$exception->getCode());
		}
		$departmentObj = new self($departmentArr[0]['title'],$departmentArr[0]['phone']);
		$departmentObj->setId($departmentArr[0]['id']);
		return $departmentObj;
	}

	static public function upgrade($id, $title, $phone, Pdo $pdo){
		$product = new Department($title, $phone);
		$product->setId($id);
		$product->update($pdo);
	}

	public function update(PDO $pdo){
		try {
			$sql = 'UPDATE departments SET
            title = :title,
            phone = :phone
            WHERE id = :id';


			$statement = $pdo->prepare($sql);
			$statement->execute([
				':title' => $this->title,
				':phone' => $this->phone,
				':id' => $this->id,
			]);

		}catch (Exception $exception){
			echo "Error updating product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
			die();
		}
	}

	static public function delete($id, PDO $pdo){
		$department = self::getById($id, $pdo);
		$department->destroy($pdo);
	}

	public function destroy($pdo){
		try {
			$sql = "DELETE FROM departments WHERE id=:id";
			$statement = $pdo->prepare($sql);
			$statement->bindValue(':id', $this->id);
			$statement->execute();
		}catch (Exception $exception){
			echo "Error deleting product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
			die();
		}
	}

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

}