<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Department</title>
</head>
<body>
<h1>Create Department</h1>
<form action="storeDepartment.php" method="post">
	<label>Entry title: <input type="text" name="title"> </label>
	<br>
	<label>Entry phone: <input type="text" name="phone"> </label>
	<br>
	<button>Save Department</button>
</form>
</body>
</html>