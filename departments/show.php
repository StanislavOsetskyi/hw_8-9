<?php
if(empty($_GET['id'])){
	header('Location:404.php');
	die();
}
require_once '../config/db.php';
require_once '../Classes/Department.php';
require_once '../classes/Teacher.php';

$id = htmlspecialchars($_GET['id']);
$department = Department::getById($id, $pdo);

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?=$department->getTitle()?></title>
</head>
<body>
<h1>Department name - <?=$department->getTitle()?>.</h1>
<ul>
	<?php foreach ($department->getTeachers() as $teacher):?>
		<li><a href="../teachers/infoTeacher.php?id=<?=$teacher->getId();?>"><?=$teacher->getFullName();?></a></li>
	<?php endforeach;?>
</ul>
<tr>

</tr>

</body>
</html>
