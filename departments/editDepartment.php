<?php
if(empty($_GET['id'])){
	header('Location:index.php');
}
require_once '../config/db.php';
require_once '../Classes/Department.php';
$department = Department::getById($_GET['id'], $pdo);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
<h1>Edit </h1>
<form action="updateDepartment.php" method="post">
	<input type="hidden" name="id" value="<?=$department->getId()?>">
	<div>
		<label>Title: <input type="text" name="title" value="<?=$department->getTitle()?>"></label>
	</div>
	<div>
		<label>Phone: <input type="text" name="phone" value="<?=$department->getPhone()?>"></label>
	</div>
	<button>Save</button>
</form>
</html>
