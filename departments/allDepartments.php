<?php
require_once '../config/db.php';
require_once '../Classes/Department.php';

$departments = Department::all($pdo);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Departments list</title>
</head>
<body>
	<div>
		<table>
			<?php foreach ($departments as $department):?>
			<tr>
				<td><?= $department->getTitle()?></td>
				<td>
					<form action="editDepartment.php" method="get">
						<input type="hidden" name="id" value="<?=$department->getId()?>">
						<button>Edit</button>
					</form>
				</td>
				<td>
					<form action="deleteDepartment.php" method="post">
						<input type="hidden" name="id" value="<?=$department->getId()?>">
						<button>Delete</button>
					</form>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</div>
	<div>
		<tr>
			<td>
				<form action="createDepartment.php" method="post">
					<button>Create new department</button>
				</form>
			</td>
		</tr>
		<td><a href="../index.php"><button>Back</button></a></td>
	</div>
</body>
</html>

