<?php
require_once '../config/db.php';
require_once '../Classes/Department.php';

if(!empty($_POST['title'])){
	$title = htmlspecialchars($_POST['title']);
	$phone = htmlspecialchars($_POST['phone']);
	$department = new Department($title,$phone);
	$department->store($pdo);
}
header('Location:../index.php');